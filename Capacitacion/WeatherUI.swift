
import UIKit


class WeatherUI: UIViewController {
    
    @IBOutlet weak var lon: UILabel!
    @IBOutlet weak var lat: UILabel!
    
    @IBOutlet weak var country: UILabel!
    @IBOutlet weak var region: UILabel!
    
    @IBOutlet weak var temp: UILabel!
    @IBOutlet weak var pressure: UILabel!
    @IBOutlet weak var humidity: UILabel!
    @IBOutlet weak var min: UILabel!
    @IBOutlet weak var max: UILabel!
    
    @IBOutlet weak var barButtonItem: UIBarButtonItem!
    
    
    @IBOutlet weak var labelTep: UILabel!
    @IBOutlet weak var labelMinTemp: UILabel!
    @IBOutlet weak var labelMaxTemp: UILabel!
    
    
    let temText: String = "Temperature "
    let minText: String = "Min "
    let maxText: String = "Max "
    
    let kelvinText: String = "(ºK)"
    let celciusText: String = "(ºC)"
    
    var kelvin:Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadWeather()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    
    func loadWeather(){
        
        var cont:Dictionary<String, String>
        cont = ["DummyData":"DummyData"]
        let params = "?id=3435910"
        
        
        DispatchQueue.main.async {
            let rest: RestApiManager = RestApiManager(service: RestApiManager.WEATHER, params: params, content: cont)!
            
            rest.doRequest(){
                //print (DataHandler.connectionResponse)
                self.parseAndShowResponse(response : DataHandler.connectionResponse)
            }
        }
        
    }
    
    
    func parseAndShowResponse(response: String){
        //dispatch_async(dispatch_get_main_queue(), {
        //DispatchQueue.async(execute: {
        var res: Dictionary<String, AnyObject> = DataHandler.stringToDictionary(json: DataHandler.connectionResponse)!
            
            //print (res)
        
            if let coord = res["main"]{
                let data: Dictionary<String, AnyObject> = coord as! Dictionary<String, AnyObject>
                print (data)
                if let v = data["temp"]{
                    self.temp.text="\(v)"
                }
                if let v = data["pressure"]{
                    self.pressure.text="\(v)"
                }
                if let v = data["humidity"]{
                    self.humidity.text="\(v)"
                }
                if let v = data["temp_min"]{
                    self.min.text="\(v)"
                }
                if let v = data["temp_max"]{
                    self.max.text="\(v)"
                }
            }
            
            if let coord = res["sys"]{
                let data: Dictionary<String, AnyObject> = coord as! Dictionary<String, AnyObject>
                //print (data)
                if let v = data["country"]{
                    self.country.text="\(v)"
                }
            }
            
            if let v = res["name"]{
                self.region.text="\(v)"
            }

            
            
            if let list = res["weather"]{
                let data: [AnyObject] = list as! [AnyObject]
                print (data)
                print (data.count)
                
                DataHandler.weatherDetails = data
            }
            
            
            if let coord = res["coord"]{
                let data: Dictionary<String, AnyObject> = coord as! Dictionary<String, AnyObject>
                print (data)
                if let lo = data["lon"]{
                    self.lon.text="\(lo)"
                    DataHandler.longitude = Double(self.lon.text!)!
                }
                if let la = data["lat"]{
                    self.lat.text="\(la)"
                    DataHandler.latitude = Double(self.lat.text!)!
                }
                
            }

            self.barButtonItem.isEnabled = true
            
            
            self.labelTep.text = self.temText + self.kelvinText
            self.labelMinTemp.text = self.minText + self.temText + self.kelvinText
            self.labelMaxTemp.text = self.maxText + self.temText + self.kelvinText
            
            self.setNeedsFocusUpdate()
            
        //})
        
    }
    
    @IBAction func changedSwitch(sender: UISwitch) {
        if kelvin{
            let f:Double = Double(self.temp.text!)!
            let c:Double = f-273.15
            self.temp.text = "\(c)"
            
            let fmin:Double = Double(self.min.text!)!
            let cmin:Double = fmin-273.15
            self.min.text = "\(cmin)"
            
            let fmax:Double = Double(self.max.text!)!
            let cmax:Double = fmax-273.15
            self.max.text = "\(cmax)"
            
            
            self.labelTep.text = self.temText + self.celciusText
            self.labelMinTemp.text = self.minText + self.temText + self.celciusText
            self.labelMaxTemp.text = self.maxText + self.temText + self.celciusText
            
        }else{
            let f:Double = Double(self.temp.text!)!
            let c:Double = f+273.15
            self.temp.text = "\(c)"
            
            let fmin:Double = Double(self.min.text!)!
            let cmin:Double = fmin+273.15
            self.min.text = "\(cmin)"
            
            let fmax:Double = Double(self.max.text!)!
            let cmax:Double = fmax+273.15
            self.max.text = "\(cmax)"
            
            
            self.labelTep.text = self.temText + self.kelvinText
            self.labelMinTemp.text = self.minText + self.temText + self.kelvinText
            self.labelMaxTemp.text = self.maxText + self.temText + self.kelvinText
        }
        
        
        kelvin = !kelvin
        self.setNeedsFocusUpdate()
        
    }
    
    @IBAction func goToMoreDetails(sender: UIButton) {
        let stroryBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextView = stroryBoard.instantiateViewController(withIdentifier: "Details") as! Details
        self.present(nextView, animated: true, completion: nil)
        
    }
    
    @IBAction func goToMap(sender: UIButton) {
        
        let stroryBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextView = stroryBoard.instantiateViewController(withIdentifier: "MapView") as! Map
        self.present(nextView, animated: true, completion: nil)

    }
}

