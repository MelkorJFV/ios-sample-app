//
//  ViewController.swift
//  Capacitacion
//
//  Created by PGOVI02 on 9/6/16.
//  Copyright © 2016 STK. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tmpLabel: UILabel!
    
    @IBOutlet weak var finalLabel: UILabel!
    
    var count:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func textChanged(sender: UITextField) {
            tmpLabel.text = sender.text
    }
    @IBAction func btnPressed(sender: UIButton) {
    
        if tmpLabel.text != ""{
            var aux:String = finalLabel.text!
            aux += "\n"
          
            if(count>=3){
                
                let alertController = UIAlertController(title: "Se llego al maximo", message: "Borrar?", preferredStyle: .alert)
                
                let cancelAction = UIAlertAction(title: "NO", style: UIAlertActionStyle.default,handler: nil)
                
                alertController.addAction(cancelAction)
                
                let OKAction = UIAlertAction(title: "SI", style: .default) { (action) in
                    aux = ""
                    self.count = 0
                    aux += "\(self.count)" + " - "
                    aux += self.tmpLabel.text!
                    
                    self.finalLabel.text = aux
                    self.count = self.count+1
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion: nil)
                
            }else{
                aux += "\(count)" + " - "
                aux += tmpLabel.text!
                
                finalLabel.text = aux
                count = count+1
            }

            
        }
        
    }

}

