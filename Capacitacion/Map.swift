import UIKit
import MapKit

class Map: UIViewController{
    
    
    let regionRadius: CLLocationDistance = 10000
    
    @IBOutlet weak var map: MKMapView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let initialLocation = CLLocation(latitude: DataHandler.latitude, longitude: DataHandler.longitude)
        self.centerMapOnLocation(location: initialLocation)
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func doBack(sender: UIBarButtonItem) {
        self.dismiss(animated: false, completion: nil)
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius * 2.0, regionRadius * 2.0)
        map.setRegion(coordinateRegion, animated: true)
    }
}
