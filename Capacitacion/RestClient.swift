import Foundation

class RestApiManager: NSObject {
    
    //MARK: Properties
    typealias ServiceResponse = (String, NSError?) ->Void
    
    let endpoint: String = "http://api.openweathermap.org/"
    let append: String = "&APPID=351b0c79cd9e7d53da8b3d823e12584e"
    
    static let WEATHER: Int = 0;
    
    let weather_append: String = "data/2.5/weather";
    
    let GET: String = "GET";
    let POST: String = "POST";
    let PUT: String = "PUT";
    
    
    var content:Dictionary<String, String> = Dictionary<String, String>()
    var service: Int = 0
    var params = ""
    var url: String=""
    var method: String = ""
    
    var data:NSData = NSData()
    var mustUseAlternateData: Bool = false
    
    init?(service: Int, params: String, content: Dictionary<String, String>){
        super.init()
        self.params = params
        self.content = content
        self.service = service
        self.doURLConfiguration(service: service)
    }
    
    func setNSData(data: NSData){
        mustUseAlternateData = true
        self.data = data
        
    }
    
    func doURLConfiguration(service: Int){
        
        switch service {
        case RestApiManager.WEATHER:
            self.url = endpoint+weather_append+params+append
            method = GET
            break
        default:
            break
        }
        
    }
    
    
    func doRequest(f:(()->())?){
        switch self.method {
        case GET:
            doGet(f: f)
            break
        case POST:
            doPost(f: f)
            break
        default:
            break
        }
    }
    
    func doGet(f:(()->())?){
        let request = NSMutableURLRequest(url: NSURL(string: url)! as URL)
        let session = URLSession.shared
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let jsonData = data {
                let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                
                DataHandler.connectionResponse = (datastring as? String)!
            }
            if let f = f{
                f()
            }
            
        })
        task.resume()
    }
    
    func doPost(f:(()->())?){
        let request = NSMutableURLRequest(url: NSURL(string: url)! as URL)
        
        // Set the method to POST
        request.httpMethod = self.method
        
        do {
            if(!mustUseAlternateData){
                var jsonBody = try JSONSerialization.data(withJSONObject: self.content, options: .prettyPrinted)
                request.httpBody = jsonBody
            }else{
                mustUseAlternateData = false
                request.httpBody = data as Data
            }
            
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            
            let session = URLSession.shared
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                if let jsonData = data {
                    
                    let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    
                    DataHandler.connectionResponse = (datastring as? String)!

                    if let f = f{
                        f()
                    }
                }
            })
            task.resume()
        } catch {
            
            
        }
    }
    
}
