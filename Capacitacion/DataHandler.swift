//
//  DataHandler.swift
//  Capacitacion
//
//  Created by PGOVI02 on 9/12/16.
//  Copyright © 2016 STK. All rights reserved.
//

import Foundation

class DataHandler{
    
    static var connectionResponse: String = ""
    
    static var weatherDetails: [AnyObject] = [AnyObject]()
    
    static var longitude: Double = 0
    static var latitude: Double = 0
    
    static func stringToDictionary(json: String) -> [String:AnyObject]? {
        
        if let data = json.data(using: String.Encoding.utf8){
            do{
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            }catch let error as NSError {
                print (error)
            }
        }
        return nil
    }

    

}
