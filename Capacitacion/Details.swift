//
//  SeleccionDeObraController.swift
//  Autogestion
//
//  Created by PGOVI02 on 7/21/16.
//  Copyright © 2016 STK. All rights reserved.
//

import UIKit
import CoreData

class Details: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    
    
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        
        self.tableView.register(cellDesign.self, forCellReuseIdentifier: "cell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataHandler.weatherDetails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "cellDesign"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath as IndexPath) as! cellDesign
        
        let data: Dictionary<String, AnyObject> = DataHandler.weatherDetails[indexPath.row] as! Dictionary<String, AnyObject>
        
        
        if let v = data["description"]{
            cell.lbl.text="\(v)"
        }
        if let v = data["icon"]{
            cell.icon.image = UIImage(named: "\(v)")
        }
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        print("TOUCHED A ROW!!! YEY!!!!")
    }
    
    
    @IBAction func doBack(sender: AnyObject) {
        self.dismiss(animated: false, completion: nil)
    }
}
